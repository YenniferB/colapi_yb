#Imagen inicial a partir dela cual se crea la nuestra
FROM node

# Definimos directorio del contenedor
WORKDIR /colapi_yb

#Añadimos contenido del proyecto en directorio de contenedor
ADD . /colapi_yb

# Puerto escucha contenedor (el mismo definido en el colapi)
EXPOSE 3000

# Comandos para lanzar nuestra API REST "colapi"
 CMD ["npm" , "start"]
