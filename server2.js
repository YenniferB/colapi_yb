//Código fuente API REST
var express = require('express');
var bodyParser = require('body-parser');
var requireJSON = ('request-json')
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = "/colapi/v3/";

var baseMLabURL = 'https://api.mlab.com/api/1/databases/colapi_db/collections/';
var apiKeyMLab = 'apiKey=CUieR_xsb_qzOS1UEljgG9pL2D-aAU5e';

app.listen(port, function(){
 console.log("API apicol escuchando en el puerto " + port + "...");
});
// GET users
app.get(URLbase + 'users',
 function(request, response) {
   console.log(URLbase);
   console.log(usersFile);
   response.send(usersFile);

   });

// GET users mLab
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET /colapi/v3/users");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado");
   var queryString = 'f={"_id":0}&';
   httpClient.get('user?'+ queryString + apiKeyMLab,
    function(err,resMlab,body){
       //console.log('Error: '+err);
       console.log('RespuestaMLab: '+ resMlab);
       console.log('body: '+body);
       var respuesta = body;
       respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
       res.send(respuesta);
    });
});

// Petición GET user con parámetros
app.get(URLbase + 'users/:id',
 function (req, res) {
   console.log("GET /colapi/v3/users/:id");
   console.log(req.params.id);
   var id = req.params.id;
   var queryString = 'q={"id":'+ id +'}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('user?'+queryString+apiKeyMLab,
   function(err,resMlab,body){
     console.log("Respuesta mLab correcta");
     var respuesta = body;
     respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
     res.send(respuesta);
   } )

});

// GET accountss mLab
app.get(URLbase + 'accounts',
 function(req, res) {
   console.log("GET /colapi/v3/accounts");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado");
   var queryString = 'f={"_id":0}&';
   httpClient.get('account?'+ queryString + apiKeyMLab,
    function(err,resMlab,body){
       console.log('Error: '+err);
       console.log('RespuestaMLab: '+ resMlab);
       console.log('body: '+body);
       var respuesta = body;
       respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
       res.send(respuesta);
    });
});

// Petición GET accounts con parametros
app.get(URLbase + 'accounts/:userid',
 function (req, res) {
   console.log("GET /colapi/v3/accounts/:userid");
   console.log(req.params.userid);
   var id = req.params.userid;
   var queryString = 'q={"userid":'+ id +'}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('account?'+queryString+apiKeyMLab,
   function(err,resMlab,body){
     console.log("Respuesta mLab correcta");
     var respuesta = body;
     respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
     res.send(respuesta);
   } )

});

// GET movements mLab
app.get(URLbase + 'movements',
 function(req, res) {
   console.log("GET /colapi/v3/movements");
   var httpClient = requestJSON.createClient(baseMLabURL);
   console.log("Cliente HTTP mLab creado");
   var queryString = 'f={"_id":0}&';
   httpClient.get('movement?'+ queryString + apiKeyMLab,
    function(err,resMlab,body){
       console.log('Error: '+err);
       console.log('RespuestaMLab: '+ resMlab);
       console.log('body: '+body);
       var respuesta = body;
       respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
       res.send(respuesta);
    });
});

// Petición GET movements con parametros
app.get(URLbase + 'movements/:id_mov',
 function (req, res) {
   console.log("GET /colapi/v3/movements/:id_mov");
   console.log(req.params.id_mov);
   var id = req.params.id_mov;
   var queryString = 'q={"id_mov":'+ id +'}&';
   var httpClient = requestJSON.createClient(baseMLabURL);
   httpClient.get('movement?'+queryString+apiKeyMLab,
   function(err,resMlab,body){
     console.log("Respuesta mLab correcta");
     var respuesta = body;
     respuesta = !err ? body : {"msg":"error en la carga de movement en mlab"}
     res.send(respuesta);
   } )
});

//POST
app.post(URLbase + 'login',
 function(req, res) {
 console.log ("POST LOGIN");
 console.log(req.body.email);
 console.log(req.body.password);
 var correo = req.body.email;
 var contrasena = req.body.password;
 for(usuario of usersFile) {
   if (usuario.email == correo){
     if (usuario.password == contrasena){
       usuario.logged = true;
       writeUserDataToFile(usersFile);
       console.log("Login correcto");
       res.send({"msg" : "Login correcto.", "idUsuario" : usuario.id});
     } else {
       res.send({"msg" : "Contraseña incorrecta.", "idUsuario" : usuario.id});
//    } else {
//       res.send({"msg" : "Correo incorrecto.", "idUsuario" : usuario.email});

//   }
     }
    }
  }
});

//LOGIN mLab
app.post(URLbase + 'loginmlab',
 function(req, res) {
   console.log("LOGINmLab/colapi/V3");
   var constasenaIng = req.body.password;
   var correo = req.body.email;
   var usuario = req.body.id;
   var clienteMlab = requestJSON.createClient(baseMLabURL);
   var queryString1 = 'q={"id":';
   var queryString2 ='}&';
  //
   clienteMlab.get('user?' + queryString1 + usuario + queryString2 + apiKeyMLab,
//    var queryString = 'f={"_id":0}&s={"id":-1}&l=1&';
//
//    clienteMlab.get('user?' + queryString + apiKeyMLab,
     function(errMLab, respuestaMLab, bodymLab) {
     console.log(bodymLab);
     var consulta = {};
     consulta = bodymLab[0];
     console.log(consulta[0]);
     console.log(consulta);
     var contrasenaCon = consulta.password;

     if(constasenaIng == contrasenaCon) {
       consulta.logged = true;
       console.log(consulta);
       console.log("Login correcto");

       var cambio = '{"$set":' + JSON.stringify(consulta) + '}';
       console.log(cambio);
       console.log(JSON.parse(cambio));

       clienteMlab.put('user?'+ queryString1 + usuario + queryString2 + apiKeyMLab, JSON.parse(cambio),
         function(errMLab2, respuestaMLab2, bodymLab2) {
         console.log('Error2 : ' + errMLab2);
         console.log('Respuesta MLab2 : ' + respuestaMLab2);
         console.log('Body2 : ' + bodymLab2);
         res.send({"msg" : "Login correcto.", "idUsuario" : consulta.email});
       });

      } else {
       res.send({"msg" : "Contraseña incorrecta.", "idUsuario" : consulta.email});
      }

   });
 });


 //LOGOUT mLab
 app.post(URLbase + 'logoutmlab',
  function(req, res) {
    console.log("LOGINmLab/colapi/V3");
    var constasenaIng = req.body.password;
    var correo = req.body.email;
    var usuario = req.body.id;
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    var queryString1 = 'q={"id":';
    var queryString2 ='}&';

    clienteMlab.get('user?' + queryString1 + usuario + queryString2 + apiKeyMLab,
      function(errMLab, respuestaMLab, bodymLab) {
      console.log(bodymLab);
      var consulta = {};
      consulta = bodymLab[0];
      console.log(consulta[0]);
      console.log(consulta);
      var contrasenaCon = consulta.password;

      if(constasenaIng == contrasenaCon) {
        consulta.logged = true;
        console.log(consulta);
        var nuevoDes = {
 //           "id" : consulta.id,
 //           "first_name" : consulta.first_name,
 //           "last_name" : consulta.last_name,
 //           "email" : consulta.email,
 //           "password" : consulta.password
          "logged": true
        };
        console.log(nuevoDes);
 //        delete consulta.logged;

        var cambio = '{"$unset":' + JSON.stringify(nuevoDes) + '}';
        console.log(cambio);
        console.log(JSON.parse(cambio));

        clienteMlab.put('user?'+ queryString1 + usuario + queryString2 + apiKeyMLab, JSON.parse(cambio),
          function(errMLab2, respuestaMLab2, bodymLab2) {
          console.log('Error2 : ' + errMLab2);
          console.log('Respuesta MLab2 : ' + respuestaMLab2);
          console.log('Body2 : ' + bodymLab2);
          res.send({"msg" : "Login correcto.", "idUsuario" : consulta.email});
        });

       } else {
        res.send({"msg" : "Contraseña incorrecta.", "idUsuario" : consulta.email});
       }

    });
  });
