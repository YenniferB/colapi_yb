var mocha = require('mocha')
var chai = require('chai')
var chaiHttp = require('chai-http')
var server = require('../server2')

var should = chai.should()

chai.use(chaiHttp) //Configurar chai mòdulo https
describe('conectividad Colombia', () => {
 it('Test BBVA', (done) => {
   chai.request('http://www.bbva.com')
       .get('/')
       .end((err,res) => {
//            console.log(res)
           res.should.have.status(200)
//            res.header['x-amz-cf-id']=='87aDzgoPWFTG8Obg2_DwgIEmTnR2T4NEYEHu5ijEf1hgEZHeuBWcMw='
           done()
       })
 });
 it('Funciona COLAPI GET', (done) => {
   chai.request('http://localhost:3000')
       .get('/colapi/v3/user/7')
       .end((err,res) => {
           res.should.have.status(200)
           done()
       })
 });

 it('Devuelve array', (done) => {
   chai.request('http://localhost:3000')
       .get('/colapi/v3/users')
       .end((err,res) => {
//          console.log(res.body)
           res.body.should.be.a('array')
           done()
       })
 });

 it('Valida primer elemento', (done) => {
   chai.request('http://localhost:3000')
       .get('/colapi/v3/users')
       .end((err, res, body) => {
         //console.log(res.body)
        //res.body[0].should.have.property('first_name')
        //res.body[0].should.have.property('last_name')
        console.log (body);
        res.body.should.be.eql("Fichero guardado")
        done()
       })
 });
});
