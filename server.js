//Código fuente API REST
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = "/colapi/v3/";

var baseMLabURL = 'https://api.mlab.com/api/1/databases/colapi_db/collections/';
var apiKeyMLab = 'apiKey=CUieR_xsb_qzOS1UEljgG9pL2D-aAU5e';

app.listen(port, function(){
 console.log("API apicol escuchando en el puerto " + port + "...");
});

app.use(bodyParser.json());

// GET users
app.get(URLbase + 'users',
 function(request, response) {
   console.log(URLbase);
   console.log(usersFile);
   response.send(usersFile);
});

//GET users a traves de mlab
app.get (URLbase + "users",
function (req, res){
  console.log("GET /apicol/v3/users");
  var httpClient = requestJASON.createCliente (baseMLabURL);
  console.log ("Cliente HTTP mLab creado");
  httpCliente.get ("user?" + apiKeyMLab,
function(err, respuestaMLab, body
  console.log ("Error: " + err);
  console.log ("Respuesta MLab: " + respuestaMLab);
  console.log ("Bady: " + body);
  var = respuesta = body;
  res.send (resuesta);
});

// Petición GET con parámetros (req.params)
app.get(URLbase + 'users/:id/:otro',
 function (req, res) {
   console.log("GET /colapi/v2/users/:id/:otro");
   console.log(req.params);
   console.log('req.params.id: ' + req.params.id);
   console.log('req.params.otro: ' + req.params.otro);
   var respuesta = req.params;
   res.send(respuesta);
});

// Petición GET con Query String (req.query)
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET con query string.");
   console.log(req.query.id);
   console.log(req.query.country);
   res.send(usersFile[pos - 1]);
   respuesta.send({"msg" : "GET con query string"});
});

// Petición POST (reg.body)
app.post(URLbase + 'users',
 function(req, res) {
   var newID = usersFile.length + 1;
   var newUser = {
     "id" : newID,
     "first_name" : req.body.first_name,
     "last_name" : req.body.last_name,
     "email" : req.body.email,
     "country" : req.body.country
   };
   usersFile.push(newUser);
   console.log(usersFile);
   res.send({"msg" : "Usuario creado correctamente: ", newUser});
 });

 // PUT
app.put(URLbase + 'users/:id',
  function(req, res){
    console.log("PUT /colapi/v2/users/:id");
    var idBuscar = req.params.id;
    var updateUser = req.body;
    for(i = 0; i < usersFile.length; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBuscar) {
        res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
      }
    }
    res.send({"msg" : "Usuario no encontrado.", updateUser});
  });

  // DELETE
  app.delete(URLbase + 'users/:id',
   function(req, res) {

     const id = req.params.id-1;
     const reg = usersFile[id];

     if(undefined != reg){
       usersFile.splice(id,1);
       res.send(204);
    } else
      res.send(404);
  });
